# ALL dependencies need to be specified here
# always included when using with spud package:
include "spud_base.rnc"

start =
  (
    ## The root node of the options dictionary for make_mesh.py.
    ##
    ## Options which are shaded blue are incomplete and require input.
    ## Greyed out options are optional.
    ## Black options are active.
    element mesh_options {
      ## Options dealing with the specification of geometry (the dimension and meshes)
      element geometry {
        ## Dimension of the problem.
        ##
        ## <b>This can only be set once!</b>
        ##
        ## This decides the autogenerated shape of all rank>0 options below (though they may still be set independently).
        element dimension {
          # a hard coded integer_value
          element integer_value {
            attribute rank {"0"},
            ( "2" )
          }
        }
      },
      ## Input/Output options
      element io {
        ## Base name for files output by the script
        element output_base_name {
          anystring
        }
      },
      ## Options describing the slab.
      element slab {
        (
          element spline {
            ## Least squares fit splint - see scipy.interpolate.splprep
            attribute name { "LeastSquaresBSpline" },
            comment
          }|
          ## Type of spline to use to describe slab surface (and any sub slab layers).
          element spline {
            ## Cubic spline - see scipy.interpolate.CubicSpline
            attribute name { "CubicSpline" },
            (
              ## Boundary conditions on cubic spline
              ##
              ## natural: the second derivative at curve ends are zero
              ##
              ## See: scipy.interpolate.CubicSpline
              element bc_type {
                attribute name { "natural" },
                comment
              }|
              ## Boundary conditions on cubic spline
              ##
              ## clamped: the first derivative at curve ends are zero
              ##
              ## See: scipy.interpolate.CubicSpline
              element bc_type {
                attribute name { "clamped" },
                comment
              }|
              ## Boundary conditions on cubic spline
              ##
              ## not-a-knot: the first & second segment at a curve end are the same polynomial
              ##
              ## See: scipy.interpolate.CubicSpline
              element bc_type {
                attribute name { "not-a-knot" },
                comment
              }
            )
          }
        ),
        ## Description of the slab surface.
        element slab_surface {
          (
            ## Data file prescribing slab surface positions.
            ## 
            ## The file should have two columns of x and y and contain at least four
            ## rows without any header or footer rows.
            element file {
              element filename {
                filename
              },
              ## Additional points to be included on the slab.
              element point {
                attribute name { xsd:string },
                real_dim_vector
              }*
            }|
            ## Prescribe the point locations.
            ##
            ## At least four points are required.
            element points {
              ## Point on the slab.
              element point {
                attribute name { xsd:string },
                real_dim_vector
              },
              ## Point on the slab.
              element point {
                attribute name { xsd:string },
                real_dim_vector
              },
              ## Point on the slab.
              element point {
                attribute name { xsd:string },
                real_dim_vector
              },
              ## Point on the slab.
              element point {
                attribute name { xsd:string },
                real_dim_vector
              },
              ## Point on the slab.
              element point {
                attribute name { xsd:string },
                real_dim_vector
              }*
            }|
            ## Python function prescribing slab positions.
            ##
            ## Functions should be of the form:
            ##
            ##  def val():
            ##     # Function code
            ##     return [[..., ...], ..., [..., ...]] # Return value
            ##
            ## The return value must have length >= 4 and each entry must be two-dimensional.
            element python {
              python_code
            }
          ),
          element layer {
            attribute name { "SlabTop" },
            slab_boundary_ids
          }
        },
        ## Describe any layers beneath the slab.
        ##
        ## Must have a basal layer.
        element sub_slab {
          ## Describe a layer of the slab.
          element layer {
            attribute name { xsd:string },
            ## Thickness of the slab layer normal to the slab length.
            element thickness {
              real
            },
            slab_region_ids,
            slab_boundary_ids?,
            slab_transects
          }*,
          ## Describe the required base layer of the slab.
          element layer {
            attribute name { "SlabBase" },
            ## Thickness of the slab layer normal to the slab length.
            element thickness {
              real
            },
            slab_region_ids,
            slab_boundary_ids?,
            slab_transects
          }
        }?
      },
      element domain {
        ## Surface of the domain (at z=0.0).
        element location {
          attribute name { "DomainSurface" },
          ## Distance to coast if trench is below the DomainSurface (z<0.0).
          ##
          ## Defaults to 0 (vertical wall from trench to coast at lhs of domain).
          ## NOTE: will be ignored if trench is at the DomainSurface (z=0.0).
          element coast_distance {
            real
          }?,
          ## Resolution at the top left of the domain.
          ##
          ## NOTE: will be ignored if the Trench is at the DomainSurface (z=0.0).
          element resolution {
            attribute name { "Coast" },
            real
          },
          ## Resolution at the top right of the domain.
          element resolution {
            attribute name { "DomainRight" },
            real
          },
          element boundary_id {
            attribute name { "DomainSurface" },
            attribute point1 { "DomainSurface::DomainRight" },
            integer
          },
          ## Boundary id between the Trench and the DomainSurface on the left hand side of the domain.
          ##
          ## NOTE: will be ignored if the Trench is at the DomainSurface (z=0.0).
          element boundary_id {
            attribute name { "CrustLeft" },
            attribute point0 { "DomainSurface::Coast" },
            attribute point1 { "Trench::SlabTop" },
            integer
          },
          ## Boundary id between the MohoBose (or UpperCrustBase if present) and the DomainSurface on the right hand side of the domain.
          element boundary_id {
            attribute name { "CrustRight" },
            attribute point0 { "DomainSurface::DomainRight" },
            integer
          },
          ## Region id of the Crust (or upper crust layer if the UpperCrustBase is specified).
          element region_id {
            attribute name { "Crust" },
            integer
          }
        },
        ## Trench end of the slab.
        element location {
          attribute name { "Trench" },
          ## Resolution at the top of the slab.
          element resolution {
            attribute name { "SlabTop" },
            real
          },
          ## Resolution at the base of the slab.
          element resolution {
            attribute name { "SlabBase" },
            real
          }
        },
        ## Depth of the lower crust.
        ##
        ## Optional but should be shallower than the MohoBase if specified.
        element location {
          attribute name { "UpperCrustBase" },
          element depth {
            real
          },
          ## Resolution at the top of the slab.
          element resolution {
            attribute name { "SlabTop" },
            real
          },
          ## Resolution at the base of the slab.
          element resolution {
            attribute name { "SlabBase" },
            real
          },
          ## Resolution at the right of the domain.
          element resolution {
            attribute name { "DomainRight" },
            real
          },
          element boundary_id {
            attribute name { "LowerCrustTop" },
            attribute point0 { "UpperCrustBase::SlabTop" },
            attribute point1 { "UpperCrustBase::DomainRight" },
            integer
          },
          element boundary_id {
            attribute name { "LowerCrustRight" },
            attribute point0 { "UpperCrustBase::DomainRight" },
            attribute point1 { "MohoBase::DomainRight" },
            integer
          },
          element region_id {
            attribute name { "LowerCrust" },
            integer
          }
        }?,
        ## Depth of the decoupling point on the slab.
        ##
        ## Optional, if not specified the MohoDepth is used.
        element location {
          attribute name { "PartialCouplingDepth" },
          element depth {
            real
          },
          ## Resolution at the top of the slab.
          element resolution {
            attribute name { "SlabTop" },
            real
          },
          ## Resolution at the base of the slab.
          ##
          ## Ignored if the slab has no layers.
          element resolution {
            attribute name { "SlabBase" },
            real
          }
        }?,
        ## Depth of the moho.
        element location {
          attribute name { "MohoBase" },
          element depth {
            real
          },
          ## Resolution at the top of the slab.
          element resolution {
            attribute name { "SlabTop" },
            real
          },
          ## Resolution at the base of the slab.
          element resolution {
            attribute name { "SlabBase" },
            real
          },
          ## Resolution at the right of the fluid zone.
          element resolution {
            attribute name { "FluidRight" },
            real
          },
          ## Resolution at the right of the domain.
          element resolution {
            attribute name { "DomainRight" },
            real
          },
          element boundary_id {
            attribute name { "FluidTop" },
            attribute point0 { "MohoBase::SlabTop" },
            attribute point1 { "MohoBase::FluidRight" },
            integer
          },
          element boundary_id {
            attribute name { "WedgeTop" },
            attribute point0 { "MohoBase::FluidRight" },
            attribute point1 { "MohoBase::DomainRight" },
            integer
          }
        },
        ## Depth of the coupling point on the slab.
        element location {
          attribute name { "CouplingDepth" },
          element depth {
            real
          },
          ## Resolution at the top of the slab.
          element resolution {
            attribute name { "SlabTop" },
            real
          },
          ## Resolution at the base of the slab.
          ##
          ## Ignored if the slab has no layers.
          element resolution {
            attribute name { "SlabBase" },
            real
          }
        },
        ## The maximum depth of the fluid zone.
        ##
        ## If set to be deeper than or equal to the maximum extent of the slab it will be adjusted to lie strictly within the slab length.
        element location {
          attribute name { "MaxFluidDepth" },
          element depth {
            real
          },
          ## Resolution at the top of the slab.
          element resolution {
            attribute name { "SlabTop" },
            real
          },
          ## Resolution at the base of the slab.
          element resolution {
            attribute name { "SlabBase" },
            real
          },
          element boundary_id {
            attribute name { "FluidRight" },
            attribute point0 { "MaxFluidDepth::SlabTop" },
            attribute point1 { "MohoBase::FluidRight" },
            integer
          },
          element region_id {
            attribute name { "WedgeFluid" },
            integer
          }
        },
        ## Optional location on rhs of domain providing a point to split boundary conditions.
        element location {
          attribute name { "UpperWedgeBase" },
          element depth {
            real
          },
          ## Resolution at right of the domain.
          element resolution {
            attribute name { "DomainRight" },
            real
          },
          element boundary_id {
            attribute name { "LowerWedgeRight" },
            attribute point0 { "UpperWedgeBase::DomainRight" },
            attribute point1 { "DomainBase::DomainRight" },
            integer
          }
        }?,
        ## Base of the domain (at z=end of the slab).
        element location {
          attribute name { "DomainBase" },
          ## Resolution at the top of the slab.
          element resolution {
            attribute name { "SlabTop" },
            real
          },
          ## Resolution at the base of the slab.
          element resolution {
            attribute name { "SlabBase" },
            real
          },
          ## Resolution at the top left of the domain.
          element resolution {
            attribute name { "DomainLeft" },
            real
          },
          ## Resolution at the top right of the domain.
          element resolution {
            attribute name { "DomainRight" },
            real
          },
          element boundary_id {
            attribute name { "MantleBase" },
            attribute point0 { "DomainBase::DomainLeft" },
            attribute point1 { "DomainBase::SlabBase" },
            integer
          },
          element boundary_id {
            attribute name { "MantleLeft" },
            attribute point0 { "Trench::SlabBase" },
            attribute point1 { "DomainBase::DomainLeft" },
            integer
          },
          element boundary_id {
            attribute name { "WedgeBase" },
            attribute point0 { "DomainBase::SlabTop" },
            attribute point1 { "DomainBase::DomainRight" },
            integer
          },
          element boundary_id {
            attribute name { "WedgeRight" },
            attribute point0 { "MohoBase::DomainRight" },
            integer
          },
          element region_id {
            attribute name { "WedgeNonFluid" },
            integer
          },
          element region_id {
            attribute name { "MantleBeneathSlab" },
            integer
          }
        },
        ## Right of the domain.
        element location {
          attribute name { "DomainRight" },
          ## Extra width on the right of the domain.
          ##
          ## Must be greater than 0.
          element extra_width {
            real
          }
        }
      },
      # Options describing the mesh.
      element mesh {
        ## Scale all resolutions by this value.
        element resolution_scale {
          real
        }?,
        ## Scale all resolutions perpendicular to and within the slab by this value.
        element slab_resolution_scale {
          real
        }?
      }
    }
  )

slab_region_ids = 
  (
    element region_id {
      attribute name { "Upper" },
      integer
    },
    element region_id {
      attribute name { "Fluid" },
      integer
    },
    element region_id {
      attribute name { "Lower" },
      integer
    }
  )

slab_boundary_ids = 
  (
    element boundary_id {
      attribute name { "Upper" },
      attribute point0 { "Trench" },
      # point 1 is unknown because it could be MohoDepth or PartialCouplingDepth
      integer
    },
    element boundary_id {
      attribute name { "Coupling" },
      # point 0 is unknown because it could be MohoDepth or PartialCouplingDepth
      attribute point1 { "CouplingDepth" },
      integer
    },
    element boundary_id {
      attribute name { "Fluid" },
      attribute point0 { "CouplingDepth" },
      attribute point1 { "MaxFluidDepth" },
      integer
    },
    element boundary_id {
      attribute name { "Lower" },
      attribute point0 { "MaxFluidDepth" },
      attribute point1 { "DomainBase" },
      integer
    }
  )

slab_transects =
  (
    ## Transect normal to the slab
    element transect_boundary_id {
      attribute name { "Trench" },
      attribute region { "Upper" },
      integer
    },
    ## Transect normal to the slab
    element transect_boundary_id {
      attribute name { "MohoBase" },
      attribute region { "Fluid" },
      integer
    },
    ## Transect normal to the slab
    element transect_boundary_id {
      attribute name { "MaxFluidDepth" },
      attribute region { "Lower" },
      integer
    },
    ## Transect normal to the slab
    element transect_boundary_id {
      attribute name { "DomainBase" },
      integer
    }
  )

