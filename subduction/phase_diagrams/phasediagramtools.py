import numpy
import csv
from scipy.interpolate import CloughTocher2DInterpolator, RectBivariateSpline
from scipy import signal

def csvtoarray(filename):
    """
    reads a fully numeric csv file that assumes the first row contains x coordinates, the first column y coord, and the remaing block z(x,y)
    """
    f = open(filename,'Ur')
    csvfile = csv.reader(f,quoting=csv.QUOTE_NONNUMERIC)
    a = [[]]
    for row in csvfile:
        a.append(row)
            
    # now separate in to column and row headers and data
    x = numpy.array(a[1][1:],dtype=float)
    y = numpy.zeros(len(a)-2) 
    z = numpy.zeros((len(y),len(x))) 

    for i in range(len(y)):
        y[i] = float(a[i+2][0]) 
        z[i] = numpy.array(a[i+2][1:],dtype=float) 

    return x, y, z
               

def splinephasediagram(T,P,F):
    TT,PP = numpy.meshgrid(T,P)
    points = numpy.array([TT.flatten(),PP.flatten()]).transpose()
    ctspline = CloughTocher2DInterpolator(points,F.flatten())
    return ctspline

def splineinterp(ctsspline,Ti,Pi):
    TTi,PPi = numpy.meshgrid(Ti,Pi)
    points = numpy.array([TTi.flatten(),PPi.flatten()]).transpose()
    Fi = ctsspline(points)
    Fi = Fi.reshape(PPi.shape)
    return Fi

def clipinterp(Fi,bounds,tol):
    #print tol
    Fi = numpy.where(Fi >  bounds.max(),bounds.max(),Fi)
    Fi = numpy.where(Fi <  bounds.min(),bounds.min(),Fi)
    return Fi

    
def gauss_kern(size, sizey=None):
    """ Returns a normalized 2D gauss kernel array for convolutions """
    size = int(size)
    if  sizey is not None:
        sizey = size
    else:
        sizey = int(size)
    x, y = numpy.mgrid[-size:size+1, -sizey:sizey+1]
    g = numpy.exp(-(x**2/float(size) + y**2/float(sizey)))
    return g / g.sum()

def blur_image(im, n, ny=None) :
    """ blurs the image by convolving with a gaussian kernel of typical
        size n. The optional keyword argument ny allows for a different
        size in the y direction.
    """
    g = gauss_kern(n, sizey=ny)
    imout = im.copy()
    improc = signal.convolve(imout, g, mode='valid')
    imout[1:-1,1:-1]=improc
    return(imout)

def multi_level_smooth_interp(x,y,im,n,nlevels,ny=None):
    """does multi-level factor of 2 smoothing and interpolation using linear interpolation and a gaussian smoother to maintain monotonicity
    """
    z = im.copy()
    # fixme:  needs better error checking for getting proper array sizes (and can't check if len(x)=len(y) and transposed
    for k in range(nlevels): # smooth then interpolate
        z= blur_image(z,1)
        I = RectBivariateSpline(x,y,z,kx=1,ky=1)
        x = numpy.linspace(x[0],x[-1],2*len(x)-1)
        y = numpy.linspace(y[0],y[-1],2*len(y)-1)
        z = I(x,y)

    return x,y,z,I

def multi_level_interp_smooth(x,y,im,n,nlevels,ny=None):
    """does multi-level factor of 2 smoothing and interpolation using linear interpolation and a gaussian smoother to maintain monotonicity
    """
    z = im.copy()
    # fixme:  needs better error checking for getting proper array sizes (and can't check if len(x)=len(y) and transposed
    for k in range(nlevels): # interpolate then smooth
        I = RectBivariateSpline(x,y,z,kx=1,ky=1)
        x = numpy.linspace(x[0],x[-1],2*len(x)-1)
        y = numpy.linspace(y[0],y[-1],2*len(y)-1)
        z = I(x,y)
        z = blur_image(z,1)

    return x,y,z,I

class PhaseDiagram:
    """ class for initializing and interpolating Hacker pseudo-phase diagrams for FH20 from Perple_X generated spreadsheets
    """

    def __init__(self,csv_filename,method="interp_smooth",smooth_width=1,levels=2):
        """initialize interpolating spline from spreadsheet data

        csv_filename     :  name of comma separated spreadsheet file from Hacker (assumes first row is T, first column P, remainder is F data)
        method       :  method for monotonic  multi_level smoother (string)
          interp_smooth - interpolate then smooth on each level (tighter features but preserves more initial artifacts)
          smooth_interp - smooth then interpolate               (smoother but broader features)
        smooth_width : width of smoother for gaussian kernel (higher is wider)
        levels       : number of refinement levels
        """

        #read CSV file and convert to numpy arrays
        T,P,F = csvtoarray(csv_filename)

        # choose smoothing method and set spline
        if method == "interp_smooth":
            Ps,Ts,Fs,self.spline = multi_level_interp_smooth(P,T,F,smooth_width,levels)
        elif method == "smooth_interp":
            Ps,Ts,Fs,self.spline = multi_level_smooth_interp(P,T,F,smooth_width,levels)
        else:
            print "Error: method ", method," not implemented"
            exit

    def eval_patch(self,P,T):
        """evaluate the smoothed bilinear spline for patch defined by arrays  P,T
        """

        F = self.spline(P,T)
        return F

    def eval(self,P,T):
        """evaluate the smoothed bilinear spline for a single point (P,T)
           Warning: no checking will be done for shape.  ONly the last point will be returned
        """

        F = self.spline(P,T)
        return F[-1,-1]

    
