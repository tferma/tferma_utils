
import numpy as np
import scipy.integrate as spint
import scipy.interpolate as spinterp
import sys

class ReferenceStates:
  """A class describing the mantle reference states with phase changes."""

  npoints = 100000  # resolution

  Ta = 1.0          # adiabat potential temperature
  Ts = 0.091        # surface temperature
  delta = 2.0       # variable alpha density power

  # The following descriptors of the phase boundaries are 
  # initialized the first time update_nonlinear is called
  p0   = []         # pressure at phase change depth
  T0   = []         # temperature at phase change depth
  rho0 = []         # lower density of phase change depth

  nits   = 0        # number of nonlinear iterations
                    # (updated in solve)
  maxits = 1000     # maximum number of allowed iterations
  atol   = 1.e-50   # absolute error
  rtol   = 1.e-10   # relative tolerance

  d = np.array([]) # vector of depths
  z = np.array([]) # vector of vertical coordinates

  rhobar = np.array([]) # reference density values vector
  pbar   = np.array([]) # reference pressure values vector
  Tbar   = np.array([]) # reference temperature values vector
  rpbar  = np.array([]) # non-linear residual of reference pressure
  rTbar  = np.array([]) # non-linear residual of reference temperature

  latent_heating = True  # include latent heating effects in the temperature
  var_alpha      = False # variable alpha with depth

  plot_convergence = False
  plotcount = 0
  
  def __init__(self, Di, epsilon, gammar, d0, gammac, drhof, w):
    """Initialization arguments: \n"""
    """  Di      - Dissipation number\n"""
    """  epsilon - epsilon non-dimensional number\n"""
    """  gammar  - Gruneisen parameter\n"""
    """  d0      - non-dimensional depth or list of depths of phase transitions (between 0 and 1)\n"""
    """  gammac  - non-dimensional Clapeyron slope(s) of phase transition(s)\n"""
    """  drhof   - fractional change in density across the phase transition(s)\n"""
    """  w       - non-dimensional width(s) of phase transition(s)"""

    # intiialize non-dimensional parameters
    self.Di      = Di
    self.epsilon = epsilon
    self.gammar  = gammar

    # transition depths, clapeyron slopes, density changes and widths
    if type(d0) is list:
      self.d0 = d0
    else:
      self.d0 = [d0]

    if type(gammac) is list:
      self.gammac = gammac
    else:
      self.gammac = [gammac]
    assert(len(self.gammac) == len(self.d0))

    if type(drhof) is list:
      self.drhof = drhof
    else:
      self.drhof = [drhof]
    assert(len(self.drhof) == len(self.d0))

    if type(w) is list:
      self.w = w
    else:
      self.w = [w]
    assert(len(self.w) == len(self.d0))

  def solve(self):
    """Solve for the reference states by iteeratively solving the relevant odes."""

    # initialize this now in case we've changed npoints
    self.d = np.linspace(0, 1, self.npoints)
    self.z = 1. - self.d

    if not self.latent_heating: self.rTbar = np.zeros(self.npoints)

    rerror = 1.0
    aerror = None
    aerror0 = None

    # iterate until the relative error reaches the set tolerance
    self.nits = 0
    continue_iterating = True
    while continue_iterating:

      if self.latent_heating:
        sbar = spint.odeint(self.dsdd_e, [0.0, self.Ta], self.d)
        self.pbar = sbar[:,0]
        self.Tbar = sbar[:,1]
      else:
        sbar = spint.odeint(self.dsdd_e, [0.0], self.d)
        self.pbar = sbar[:,0]
        self.Tbar = self.Tbar_e(self.d)

      dsddo = self.dsdd_e([self.pbar, self.Tbar] , self.d)

      self.update_nonlinear()

      dsdd = self.dsdd_e([self.pbar, self.Tbar] , self.d)

      self.rpbar = dsdd[0]-dsddo[0]
      aerror = sum((self.rpbar)**2)
      if self.latent_heating: 
        self.rTbar = dsdd[1] - dsddo[1]
        aerror += sum((self.rTbar)**2)
      aerror = aerror**0.5
      if self.nits == 0: aerror0 = aerror
      rerror = aerror/aerror0

      if self.plot_convergence:
        self.plot()

      continue_iterating = rerror > self.rtol and \
                           aerror > self.atol and \
                           self.nits + 1 < self.maxits
      print "it = %d, rerror = %1.5e, aerror = %1.5e"%(self.nits, rerror, aerror)
      self.nits += 1

    # throw an error if we've failed to converge
    if rerror > self.rtol and aerror > self.atol:
      print "solve failed to converge"
      print "  rerror / rtol = %1.5e / %1.5e "%(rerror, self.rtol)
      print "  aerror / atol = %1.5e / %1.5e "%(aerror, self.atol)
      sys.exit(1)

    # update the nonlinear phase transition parameters
    self.update_nonlinear()

    # retrieve the correct rhobar given the final reference pressure and temperature
    self.rhobar = self.rhobar_e(self.d, p=self.pbar, T=self.Tbar)
    self.dTdd = self.dTdd_e(self.d, self.rhobar, self.pbar, self.Tbar)

  def plot(self):
    import pylab

    pylab.figure(0)
    pylab.clf()
    pylab.ylabel("z")
    pylab.grid(axis='y')
    pylab.plot(self.rhobar_e(self.d, p=self.pbar, T=self.Tbar), self.z, 'k')
    pylab.xlabel("rhobar", color='k')
    pylab.savefig("rhobar_"+`self.plotcount`.zfill(5)+".png") 

    pylab.figure(1)
    pylab.clf()
    pylab.ylabel("z")
    pylab.grid(axis='y')
    pylab.plot(self.pbar, self.z, 'k')
    pylab.xlabel("pbar", color='k')
    pylab.tick_params('x', colors='k')
    pylab.twiny()
    pylab.plot(self.rpbar, self.z, 'b')
    pylab.tick_params('x', colors='b')
    pylab.xlabel("residual", color='b')
    pylab.savefig("pbar_"+`self.plotcount`.zfill(5)+".png") 

    pylab.figure(2)
    pylab.clf()
    pylab.ylabel("z")
    pylab.grid(axis='y')
    pylab.plot(self.Tbar-self.Ta, self.z, 'k')
    pylab.xlabel("Tbar - Ta", color='k')
    pylab.tick_params('x', colors='k')
    pylab.twiny()
    pylab.plot(self.rTbar, self.z, 'b')
    pylab.tick_params('x', colors='b')
    pylab.xlabel("residual", color='b')
    pylab.savefig("Tbar_"+`self.plotcount`.zfill(5)+".png") 

    pylab.figure(3)
    pylab.clf()
    pylab.ylabel("z")
    pylab.grid(axis='y')
    pylab.plot(self.Gamma_e(self.pbar, self.Tbar, 0), self.z, 'k')
    pylab.xlabel("Gamma_0", color='k')
    pylab.tick_params('x', colors='k')
    pylab.twiny()
    pylab.plot(self.pi_e(self.pbar, self.Tbar, 0), self.z, 'b')
    pylab.tick_params('x', colors='b')
    pylab.xlabel("pi_0", color='b')
    pylab.savefig("Gammabar_"+`self.plotcount`.zfill(5)+".png") 

    pylab.figure(4)
    pylab.clf()
    pylab.ylabel("z")
    pylab.grid(axis='y')
    pylab.plot(self.alpha_e(self.d, self.rhobar_e(self.d, p=self.pbar, T=self.Tbar), self.pbar, self.Tbar), self.z, 'k')
    pylab.xlabel("alpha", color='k')
    pylab.tick_params('x', colors='k')
    pylab.twiny()
    pylab.plot(self.cp_e(self.rhobar_e(self.d, p=self.pbar, T=self.Tbar), self.pbar, self.Tbar), self.z, 'b')
    pylab.tick_params('x', colors='b')
    pylab.xlabel("cp", color='b')
    pylab.savefig("alphacp_"+`self.plotcount`.zfill(5)+".png") 

    self.plotcount += 1

  def rhobar_e(self, d, p, T, n=None):
    """An Adams-Williamson expression for the reference density."""
    if self.var_alpha:
      rhobarbar = (self.delta*self.Di*d/self.gammar + 1.0)**(1./self.delta)
    else:
      rhobarbar = np.exp(self.Di*d/self.gammar)
    phasejump = 0.0
    nl = len(self.p0)  # if p0 hasn't been defined yet then we won't use it
    if n is not None: 
      assert(n <= nl)
      nl = n
    for i in xrange(nl):
      phasejump += self.drhof[i]*self.rho0[i]*self.Gamma_e(p, T, i)
    return rhobarbar + phasejump

  def Tbar_e(self, d):
    """An expression for the adiabat excluding latent heating."""
    if self.var_alpha:
      Tbar = self.Ta*((self.delta*self.Di*d/self.gammar + 1.0)**(self.gammar/self.delta))
    else:
      Tbar = self.Ta*np.exp(self.Di*d)
    return Tbar

  def dTdd_e(self, d, rho, p, T):
    """An expression for the gradient of the adiabat with depth including latent heating."""
    if self.latent_heating or self.var_alpha:
      dTdd = self.Di*self.alpha_e(d, rho, p, T)*T/self.cp_e(rho, p, T)
    else:
      dTdd = self.Di*T
    return dTdd

  def pi_e(self, p, T, i):
    """An expression for the overpressure"""
    return (p - self.p0[i] - self.gammac[i]*(T - self.T0[i]))

  def Gamma_e(self, p, T, i):
    """An expression for the phase indicator function"""
    return 0.5*(1.+np.tanh(2.*self.epsilon*self.pi_e(p, T, i)/self.w[i]))

  def dGammadpi_e(self, p, T, i):
    """An expression for the derivative of the phase indicator function wrt the overpressure"""
    Gamma = self.Gamma_e(p, T, i)
    return (4.*self.epsilon/self.w[i])*(Gamma - Gamma**2)

  def alpha_e(self, d, rho, p, T):
    """An expression for the thermal expansivity (including latent heating effects)"""
    if self.var_alpha:
      alphabar = (self.delta*self.Di*d/self.gammar + 1.0)**(-1.0)
    else:
      alphabar = 1.0
    latent = 0.0
    for i in xrange(len(self.p0)):
      latent += (1./self.epsilon)*rho*self.gammac[i]*self.drhof[i]*self.rho0[i]*self.dGammadpi_e(p, T, i)/ \
               ((1.+self.drhof[i])*self.rho0[i]*self.rho0[i])
    return alphabar + latent

  def cp_e(self, rho, p, T):
    """An expression for the heat capacity (including latent heating effects)"""
    latent = 0.0
    for i in xrange(len(self.p0)):
      latent += self.Di*self.gammac[i]*self.gammac[i]*T*self.drhof[i]*self.rho0[i]*self.dGammadpi_e(p, T, i)/ \
                ((1.+self.drhof[i])*self.rho0[i]*self.rho0[i])
    return 1.0 + latent

  def dsdd_e(self, s, d):
    """An expression for the pressure and thermal odes"""
    p   = s[0]
    if self.latent_heating:
      T = s[1]
    else:
      T = self.Tbar_e(d)
    rho = self.rhobar_e(d, p, T)
    dpdd = (1./self.epsilon)*rho
    dsdd = [dpdd]
    if self.latent_heating: dsdd.append(self.dTdd_e(d, rho, p, T))
    return dsdd

  def update_nonlinear(self):
    """Update the nonlinear phase transition parameters based on the current temperature and pressure"""
    if self.latent_heating:
      Tbari = spinterp.InterpolatedUnivariateSpline(self.d, self.Tbar)
      self.T0 = Tbari(self.d0)
    else:
      self.T0 = self.Tbar_e(np.array(self.d0))

    pbari = spinterp.InterpolatedUnivariateSpline(self.d, self.pbar)
    self.p0 = pbari(self.d0)

    self.rho0 = []
    for i in xrange(len(self.d0)): 
      # NOTE: double check logic below if using more than 1 phase change
      self.rho0.append(self.rhobar_e(self.d0[i], self.p0[i], self.T0[i], i))


